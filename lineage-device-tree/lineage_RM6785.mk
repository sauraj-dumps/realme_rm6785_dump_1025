#
# Copyright (C) 2023 The LineageOS Project
#
# SPDX-License-Identifier: Apache-2.0
#

# Inherit from those products. Most specific first.
$(call inherit-product, $(SRC_TARGET_DIR)/product/core_64_bit.mk)
$(call inherit-product, $(SRC_TARGET_DIR)/product/full_base_telephony.mk)

# Inherit some common Lineage stuff.
$(call inherit-product, vendor/lineage/config/common_full_phone.mk)

# Inherit from RM6785 device
$(call inherit-product, device/realme/RM6785/device.mk)

PRODUCT_DEVICE := RM6785
PRODUCT_NAME := lineage_RM6785
PRODUCT_BRAND := realme
PRODUCT_MODEL := RM6785
PRODUCT_MANUFACTURER := realme

PRODUCT_GMS_CLIENTID_BASE := android-oppo

PRODUCT_BUILD_PROP_OVERRIDES += \
    PRIVATE_BUILD_DESC="lineage_RM6785-userdebug 13 TQ1A.230105.001.A2 22 dev-keys"

BUILD_FINGERPRINT := realme/lineage_RM6785/RM6785:13/TQ1A.230105.001.A2/22:userdebug/dev-keys
