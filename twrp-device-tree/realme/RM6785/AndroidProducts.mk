#
# Copyright (C) 2023 The Android Open Source Project
# Copyright (C) 2023 SebaUbuntu's TWRP device tree generator
#
# SPDX-License-Identifier: Apache-2.0
#

PRODUCT_MAKEFILES := \
    $(LOCAL_DIR)/omni_RM6785.mk

COMMON_LUNCH_CHOICES := \
    omni_RM6785-user \
    omni_RM6785-userdebug \
    omni_RM6785-eng
